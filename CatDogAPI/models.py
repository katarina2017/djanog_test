from django.db import models
from django.contrib.auth.models import User


class Cat(models.Model):
    name = models.CharField(max_length=30)
    birthday = models.DateField(blank=True, null=True)
    owner = models.ForeignKey(User, to_field='id')

    def __str__(self):
        return self.name


class Dog(models.Model):
    name = models.CharField(max_length=30)
    birthday = models.DateField(blank=True, null=True)
    owner = models.OneToOneField(User, to_field='id')

    def __str__(self):
        return self.name