from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from CatDogAPI.models import Cat, Dog


class CatResource(ModelResource):
    class Meta:
        queryset = Cat.objects.all()
        resource_name = 'cat'
        fields = ['name', 'birthday', 'owner']
        authorization = Authorization()


class DogResource(ModelResource):
    class Meta:
        queryset = Dog.objects.all()
        resource_name = 'dog'
        fields = ['name', 'birthday', 'owner']
        authorization = Authorization()