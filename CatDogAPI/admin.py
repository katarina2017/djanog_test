from django.contrib import admin
from CatDogAPI.models import Cat, Dog, User

admin.site.unregister(User)
admin.site.register(Cat)
admin.site.register(Dog)
