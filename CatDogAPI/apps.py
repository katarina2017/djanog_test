from django.apps import AppConfig


class CatdogapiConfig(AppConfig):
    name = 'CatDogAPI'
